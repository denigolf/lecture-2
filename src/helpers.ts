import IMovie from './IMovie';

export function getData(url: string): Promise<Object> {
    return fetch(url).then((response) => response.json());
}

export function getRandomInt(min: number, max: number): number {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

export function clearContainer(container: HTMLElement): void {
    if (container) {
        container.innerHTML = '';
    }
}

export function updateIcon(movieSVG: SVGSVGElement, fillColor: string): void {
    movieSVG.setAttribute('fill', fillColor);
}

export function mapMovie(movie: IMovie): IMovie {
    return {
        id: movie.id,
        overview: movie.overview,
        release_date: movie.release_date,
        poster_path: movie.poster_path,
        backdrop_path: movie.backdrop_path,
        original_title: movie.original_title,
    };
}

export function mapMovies(movies: IMovie[]): IMovie[] {
    const IMovieArray: IMovie[] = movies.map((movie: IMovie) => {
        return mapMovie(movie);
    });

    return IMovieArray;
}
