export default interface IMovie {
    id: number;
    overview: string;
    release_date: string;
    poster_path: string;
    backdrop_path: string;
    original_title: string;
}
