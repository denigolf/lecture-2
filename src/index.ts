import IMovie from './IMovie';
import { saveToLocalStorage, removeFromLocalStorage } from './localStorage';
import {
    getData,
    getRandomInt,
    mapMovies,
    mapMovie,
    clearContainer,
    updateIcon,
} from './helpers';

export async function render(): Promise<void> {
    const API_KEY: string = 'cf6a7215fd6d8021057dfb478cc2388a';

    const IMG_API: string = 'https://image.tmdb.org/t/p/original';
    const POPULAR_API: string = `https://api.themoviedb.org/3/movie/popular?api_key=${API_KEY}&language=en-US`;
    const UPCOMING_API: string = `https://api.themoviedb.org/3/movie/upcoming?api_key=${API_KEY}&language=en-US`;
    const TOP_RATED_API: string = `https://api.themoviedb.org/3/movie/top_rated?api_key=${API_KEY}&language=en-US`;
    const DISCOVER_API: string = `https://api.themoviedb.org/3/discover/movie?api_key=${API_KEY}&language=en-US&sort_by=popularity.desc&include_adult=false&include_video=false&with_watch_monetization_types=flatrate`;
    const SEARCH_API: string = `https://api.themoviedb.org/3/search/movie?api_key=${API_KEY}&include_adult=false&language=en-US`;

    const loadMoreButton: HTMLButtonElement = <HTMLButtonElement>(
        document.getElementById('load-more')
    );
    const upcomingButton: HTMLInputElement = <HTMLInputElement>(
        document.getElementById('upcoming')
    );
    const topRatedButton: HTMLInputElement = <HTMLInputElement>(
        document.getElementById('top_rated')
    );
    const popularButton: HTMLInputElement = <HTMLInputElement>(
        document.getElementById('popular')
    );
    const submitButton: HTMLButtonElement = <HTMLButtonElement>(
        document.getElementById('submit')
    );
    const offcanvasRight: HTMLButtonElement = <HTMLButtonElement>(
        document.getElementById('offcanvasRight')
    );

    let fullUrl: string = '';
    let page: number = 0;

    function offcanvasRightHandler(): void {
        renderMovies(fullUrl);
    }

    function loadMoreButtonHandler(): void {
        renderMore(fullUrl);
    }

    function upcomingButtonHandler(): void {
        renderMovies(UPCOMING_API);
    }

    function topRatedButtonHandler(): void {
        renderMovies(TOP_RATED_API);
    }

    function popularButtonHandler(): void {
        renderMovies(POPULAR_API);
    }

    function submitButtonHandler(): void {
        renderMoviesSearch(SEARCH_API);
    }

    function iconSvgHandler(
        iconSvg: SVGSVGElement,
        movieId: number,
        fillColor: string,
        likedColor: string
    ): void {
        if (iconSvg.getAttribute('fill') === fillColor) {
            updateIcon(iconSvg, likedColor);
            saveToLocalStorage(movieId);
        } else {
            updateIcon(iconSvg, fillColor);
            removeFromLocalStorage(movieId);
        }
        renderFavoriteMovies();
    }

    window.onload = () => {
        renderMovies(POPULAR_API);
        renderRandomMovie();
        renderFavoriteMovies();
    };

    loadMoreButton?.addEventListener('click', loadMoreButtonHandler);

    upcomingButton?.addEventListener('click', upcomingButtonHandler);

    topRatedButton?.addEventListener('click', topRatedButtonHandler);

    popularButton?.addEventListener('click', popularButtonHandler);

    submitButton?.addEventListener('click', submitButtonHandler);

    offcanvasRight.addEventListener('blur', offcanvasRightHandler);

    async function renderFavoriteMovies(): Promise<void> {
        const container: HTMLDivElement = <HTMLDivElement>(
            document.getElementById('favorite-movies')
        );
        const movieIds: number[] = JSON.parse(
            localStorage.getItem('favoriteMovies') || '[]'
        );

        if (container) {
            clearContainer(container);
        }

        for (let movieId of movieIds) {
            const url: string = `https://api.themoviedb.org/3/movie/${movieId}?api_key=cf6a7215fd6d8021057dfb478cc2388a&language=en-US`;

            const movie: any = await getData(url);
            const mappedMovie: IMovie = mapMovie(movie);

            const divClasses: string[] = ['col-12', 'p-2'];
            renderMovie(mappedMovie, container, divClasses);
        }
    }

    async function renderRandomMovie(): Promise<void> {
        const randomPageNumber: number = getRandomInt(1, 500);
        const randomMovieNumber: number = getRandomInt(1, 20);

        const movies: any = await getData(
            `${DISCOVER_API}&page=${randomPageNumber}`
        );

        const mappedMovies: IMovie[] = mapMovies(movies.results);
        const randomMovie: IMovie = mappedMovies[randomMovieNumber];

        if (!randomMovie) renderRandomMovie();

        const randomMovieElement: HTMLElement | null =
            document.getElementById('random-movie');
        const randomMovieNameElement: HTMLHeadingElement = <HTMLHeadingElement>(
            document.getElementById('random-movie-name')
        );
        const randomMovieDescriptionElement: HTMLParagraphElement = <
            HTMLParagraphElement
        >document.getElementById('random-movie-description');

        if (
            randomMovieElement &&
            randomMovieNameElement &&
            randomMovieDescriptionElement
        ) {
            if (!randomMovie.backdrop_path) renderRandomMovie();

            randomMovieElement.style.backgroundImage = `url("${IMG_API}${randomMovie.backdrop_path}")`;
            randomMovieElement.style.backgroundSize = 'cover';
            randomMovieElement.style.backgroundPosition = `center`;
            randomMovieElement.style.height = `75vh`;
            randomMovieNameElement.textContent = randomMovie.original_title;
            randomMovieDescriptionElement.textContent = randomMovie.overview;
        }
    }

    function renderFavoriteIcon(
        node: HTMLElement,
        isFilled: boolean,
        movieId: number
    ): void {
        const fillColor: string = '#ff000078';
        const likedColor: string = 'red';
        const iconSvg: SVGSVGElement = document.createElementNS(
            'http://www.w3.org/2000/svg',
            'svg'
        );
        const iconPath: SVGPathElement = document.createElementNS(
            'http://www.w3.org/2000/svg',
            'path'
        );

        const iconPathClassList: string[] = [
            'bi',
            'bi-heart-fill',
            'position-absolute',
            'p-2',
        ];

        iconSvg.setAttribute('viewBox', '0 -2 18 22');
        iconSvg.setAttribute('stroke', 'red');
        iconSvg.setAttribute('fill', fillColor);
        iconSvg.setAttribute('width', '50px');
        iconSvg.setAttribute('height', '50px');
        iconSvg.style.cursor = 'pointer';
        iconSvg.classList.add(...iconPathClassList);

        iconPath.setAttribute('stroke', 'red');
        iconPath.setAttribute(
            'd',
            'M8 1.314C12.438-3.248 23.534 4.735 8 15-7.534 4.736 3.562-3.248 8 1.314z'
        );

        iconSvg.addEventListener('click', () =>
            iconSvgHandler(iconSvg, movieId, fillColor, likedColor)
        );

        const isFavorite: boolean = JSON.parse(
            localStorage.getItem('favoriteMovies') || '[]'
        ).includes(movieId);

        if (isFavorite) {
            iconSvg.setAttribute('fill', 'red');
        }

        if (isFilled) {
            iconSvg.setAttribute('fill', 'red');
        }

        iconSvg.appendChild(iconPath);
        node.appendChild(iconSvg);
    }

    function renderMoviesSearch(url: string): void {
        const container: HTMLDivElement = <HTMLDivElement>(
            document.getElementById('film-container')
        );

        page = 0;

        if (container) {
            clearContainer(container);
        }

        const inputElement: HTMLInputElement = <HTMLInputElement>(
            document.getElementById('search')
        );
        const inputValue: string = inputElement?.value;
        fullUrl = `${url}&query=${inputValue}`;

        renderMore(fullUrl);
    }

    function renderMovies(url: string): void {
        const container: HTMLDivElement = <HTMLDivElement>(
            document.getElementById('film-container')
        );

        page = 0;

        if (container) {
            clearContainer(container);
        }

        renderMore(url);
    }

    async function renderMore(url: string): Promise<void> {
        const container: HTMLDivElement = <HTMLDivElement>(
            document.getElementById('film-container')
        );
        const currentPage: number = ++page;

        fullUrl = `${url}&page=${currentPage}`;

        const movies: any = await getData(fullUrl);
        const mappedMovies: IMovie[] = mapMovies(movies.results);

        for (let movie of mappedMovies) {
            const divClasses = ['col-lg-3', 'col-md-4', 'col-12', 'p-2'];
            renderMovie(movie, container, divClasses);
        }
    }

    function renderMovie(
        movie: IMovie,
        container: HTMLDivElement,
        divClasses: string[]
    ): void {
        if (movie.poster_path) {
            const imgURL: string = `${IMG_API}${movie.poster_path}`;

            const movieWrapper: HTMLDivElement = <HTMLDivElement>(
                document.createElement('div')
            );
            const movieWrapperClassList: string[] = [...divClasses];
            movieWrapper.classList.add(...movieWrapperClassList);

            const card: HTMLDivElement = <HTMLDivElement>(
                document.createElement('div')
            );
            const cardClassList: string[] = ['card', 'shadow-sm'];
            card.classList.add(...cardClassList);

            const img: HTMLImageElement = document.createElement('img');
            img.src = imgURL;

            const cardBody: HTMLDivElement = <HTMLDivElement>(
                document.createElement('div')
            );
            const cardBodyClassList: string[] = ['card-body'];
            cardBody.classList.add(...cardBodyClassList);

            const cardBodyP: HTMLParagraphElement = document.createElement('p');
            const cardBodyPClassList: string[] = ['card-text', 'truncate'];
            cardBodyP.classList.add(...cardBodyPClassList);
            cardBodyP.textContent = movie.overview;

            const cardBodyDate: HTMLDivElement = <HTMLDivElement>(
                document.createElement('div')
            );
            const cardBodyDateClassList: string[] = [
                'd-flex',
                'justify-content-between',
                'align-items-center',
            ];
            cardBodyDate.classList.add(...cardBodyDateClassList);

            const cardBodySmall: HTMLElement = document.createElement('small');
            cardBodySmall.classList.add('text-muted');
            cardBodySmall.textContent = movie.release_date;
            cardBodyDate.appendChild(cardBodySmall);

            cardBody.appendChild(cardBodyP);

            cardBody.appendChild(cardBodyDate);

            card.appendChild(img);

            renderFavoriteIcon(card, false, movie.id);

            card.appendChild(cardBody);

            movieWrapper.appendChild(card);

            if (container) {
                container.append(movieWrapper);
            }
        }
    }
}
