export function saveToLocalStorage(movieId: number): void {
    if (!localStorage.getItem('favoriteMovies')) {
        localStorage.setItem('favoriteMovies', '[]');
    }

    const movieIds: number[] = JSON.parse(
        localStorage.getItem('favoriteMovies') || '[]'
    );

    if (!movieIds.includes(movieId)) {
        movieIds.push(movieId);
    }

    localStorage.setItem('favoriteMovies', JSON.stringify(movieIds));
}

export function removeFromLocalStorage(movieId: number) {
    if (!localStorage.getItem('favoriteMovies')) {
        localStorage.setItem('favoriteMovies', '[]');
    }

    let movieIds: number[] = JSON.parse(
        localStorage.getItem('favoriteMovies') || '[]'
    );

    movieIds = movieIds.filter((item: number) => item !== movieId);

    localStorage.setItem('favoriteMovies', JSON.stringify(movieIds));
}
